#' Create Travel Survey
#'
#' Creates a travel survey object for use with the HOT model using
#' three tables, person, house and trip.  If participation data are
#' available an optional fourth table, location, may be used.  As with
#' most travel survey data sets the person table contains information
#' at the individual respondent level, the house table contains
#' information at the house level and the trip table contains data on
#' trips taken by individuals.
#'
#' @param person a data frame with four columns; houseID, subjectID,
#'     sex, and age.  houseID and subjectID must have character class,
#'     sex must be a factor with levels "M" and "F" in that order, age
#'     must be a factor with levels "child", "adult", "senior" in that
#'     order.
#' @param trip a data frame with four columns; houseID, subjectID,
#'     duration, and mode.  The hosueID and subjectID variables must
#'     have character class.  It is these variables that are used to
#'     merge across tables, e.g., person and trip.  The duration
#'     variable must have class numeric and represent the trip
#'     duration in minutes.  The mode variable must be of class factor
#'     with levels "walk", "cycle" and "other" in that order.
#' @param house a data frame with four columns; houseID, location, and
#'     year.  The houseID must be of class character.  It is this
#'     variable that is used to merge with other tables.  The location
#'     variable must be of class factor with levels describing the
#'     distinct location labels, e.g., state, county, or district
#'     names.  The year variable must be of class character and
#'     describe the year in which the survey data were collected.
#' @param location an optional data frame with variables location and
#'     participation.  The location variable must be of class factor
#'     with levels identical to those in the house table.  The
#'     participation variable must be of class numeric and represent
#'     the proportion of the population (in the given location) that
#'     participates in walking or cycling regularly.
#' @param frequency If participation data are unavailable use this
#'     parameter in place of the location table.  frequency must be
#'     numeric and represents the expected frequency at which active
#'     travelers are active across all locations.
#'     
#' @return A TravelSurvey object.
#' 
#' @note For more information see the tutorial at
#'     https://ghi-uw.gitlab.io/hot-tutorial/.
#' 
#' @export
createTravelSurvey <- function(person, house, trip, location = NA, frequency = 5/7){
    
    house.not.person.index <- which(!(house$houseID %in% person$houseID))
    if( length(house.not.person.index) > 0 ){
        house <- house[-house.not.person.index,]
    }

    person.not.house.index <- which(!(person$houseID %in% house$houseID))
    if( length(person.not.house.index) > 0 ){
        person <- person[-person.not.house.index,]
    }

    person.trip <- left_join(person, trip, by = c("houseID", "subjectID"))
    trip <- person.trip %>% select(houseID, subjectID, duration, mode)
    
    if(!is.data.frame(location)){

        message("Location is not a data frame.")

        ts.temp <- new("TravelSurvey",
                       person = person,
                       house = house,
                       trip = trip,
                       location = data.frame(location = factor(levels(house$location)), participation = 1))

        prevalence.df <- getPrevalence(ts.temp)

        locpub <- within(prevalence.df,{
            location = prevalence.df$location
            participation = ifelse(prevalence.df$p1/frequency < 1, prevalence.df$p1/frequency, 1)
        })

        location <- locpub %>% select(location, participation)
    }

    ts <- new("TravelSurvey",
                   person = person,
                   house = house,
                   trip = trip,
                   location = location)



    return(ts)
}
