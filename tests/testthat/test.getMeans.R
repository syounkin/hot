context("checking getMeans")

test_that("means are correct", {

    ts <- readRDS(file = system.file("example.rds", package = "HOT"))

    result <- getMeans(ts, activeTravelers = FALSE)

    expect_equal(as.numeric(result[,"walk"]), 50/15)
    expect_equal(as.numeric(result[,"cycle"]), 60/15)
    expect_equal(as.numeric(result[,"other"]), 100/15)

    result <- getMeans(ts, activeTravelers = TRUE)

    expect_equal(as.numeric(result[,"walk"]), 50/8)
    expect_equal(as.numeric(result[,"cycle"]), 60/8)
    expect_equal(as.numeric(result[,"other"]), 0/8)

})
