context("checking duration by mode")

test_that("Everyone is included and two subjects have zero trips", {

    ts <- readRDS(file = system.file("example.rds", package = "HOT"))

    result <- getDurationByMode(ts)

    n1 <- nrow(result)

    n2 <- nrow(ts@person)

    expect_equal(n1, n2)

    totalDuration <- rowSums(result[,c("walk","cycle","other")])

    expect_equal(sum(totalDuration == 0), 2)

})
