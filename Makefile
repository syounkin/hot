R-packages: packages1 packages2 packages3 packages4 packages5 packages6

pdf: ./NAMESPACE
	R ${R_OPTS} -e 'library(roxygen2);roxygenize("~/HOT/")'
	R CMD Rd2pdf --no-preview --force ~/HOT/

test:
	R -e 'library("devtools"); devtools::test()'

redocument:
	R -e 'library("devtools"); library("roxygen2"); document()'

check:
	$(MAKE) redocument
	R CMD build .
	R CMD check --as-cran HOT_*.*.*.tar.gz

rebuild:
	$(MAKE) redocument
	cd && R -e 'library("devtools"); install_gitlab("ghi-uw/hot", force = TRUE, upgrade = "always")';

rebuildLocal:
	$(MAKE) redocument
	cd && R -e 'library("devtools"); install("~/HOT/", force = TRUE, upgrade = FALSE)';

rebuild4deploy:
	$(MAKE) redocument
	cd && R -e 'library("devtools"); pat <- scan(file= "~/HOT/config", "character"); install_github("syounkin/HOT-mirror@devel", auth_token = pat, force = TRUE, upgrade = "never")';

HOTData:
	cd ~/HOT-Tool/R && R -e 'source("getHOTData.R"); getLondon(); getFrance()'

London:
	$(MAKE) rebuild
	cd && cd ~/CUSSH && $(MAKE) London

London.B:
	$(MAKE) rebuild
	cd && cd ~/CUSSH && $(MAKE) London.B

London.new:
	$(MAKE) rebuild
	cd && cd ~/CUSSH && R -e "rmarkdown::render('./R/London.new.Rmd', output_file = '../html/London.new.html')"
	cd && cd ~/CUSSH && rm -vf ./London.new.md

London.all:
	$(MAKE) rebuild
	cd && cd ~/CUSSH && $(MAKE) London
	cd && cd ~/CUSSH && $(MAKE) London.B

HOTApp:
	cd ~/HOT-Tool/R && R -e 'library("shiny"); runApp("HOT", launch.browser = TRUE)'

manual:
	rm -f HOT.pdf
	R CMD Rd2pdf ~/HOT/
	cp -v ~/HOT/HOT.pdf ~/box/HOT/misc/

packages1:
	R -e 'install.packages(c("devtools", "tidyverse", "reshape2","rgdal","rgeos","maptools","plotly"), repos = "https://cloud.r-project.org")'

packages2:
	R -e 'install.packages(c("xtable","formattable","spatstat","ggpubr"), repos = "https://cloud.r-project.org")'

packages3:
	R -e 'install.packages(c("raster", "sf", "reactable"), repos = "https://cloud.r-project.org")'

packages4:
	R -e 'install.packages(c("shiny", "shinydashboard", "shinyjs", "leaflet","shinyWidgets", "DT"), repos = "https://cloud.r-project.org")'

packages5:
	R -e 'install.packages(c("gtsummary","webshot2","huxtable","DT","bookdown","survey","mixdist","writexl"), repos = "https://cloud.r-project.org")'

packages6:
	R -e 'install.packages(c("fitdistrplus"), repos = "https://cloud.r-project.org")'
