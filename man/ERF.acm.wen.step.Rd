% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/riskFunctions.R
\name{ERF.acm.wen.step}
\alias{ERF.acm.wen.step}
\title{All-Cause Mortality ERF: Step Function, per Wen et al}
\usage{
ERF.acm.wen.step(x)
}
\arguments{
\item{x}{Activity (MET hrs/week)}
}
\value{
Relative Risk (Value between 0 and 1)
}
\description{
Calculates risk associated with a given amount of activity (MET hrs/week), based on five categories of physical activity volume. This risk calculation method was used by Wen et al. Assumes step function shape between Arem's hazard ratio points.
}
